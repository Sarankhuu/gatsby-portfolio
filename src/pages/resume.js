import React from "react"
import { Link, graphql,useStaticQuery } from "gatsby"
import { css } from "@emotion/core"
import styled from "@emotion/styled"

import Layout from "../components/layout/layout"
import firebase from "gatsby-plugin-firebase"

const MainContent = styled.div`
  margin: 0 auto;
  max-width: 860px;
  padding: 1.45rem 1.0875rem;
  font-family: "Lato";
  font-weight: 300;
  font-size: 16px;
  height: 78vh;
`
const TitleSection = styled.div`
  float: left;
  width: 30%;
`
const Description = styled.div`
  float: left;
  width: 70%

  
`

const ArticleDate = styled.h5`
  display: inline;
  color: #606060;
`

const MarkerHeader = styled.h4`
  display: inline;
  font-family: "Raleway";
  color: #333;
`

const Company = styled.h5`
  display: inline;
  color: #606060;
`

const IndexPage = () => {
  const [data, setData] = React.useState(null)
    React.useEffect(() => {
        firebase
          .database()
          .ref("/experience")
          .once("value")
          .then(snapshot => {
            setData(snapshot.val())
          })
      }, [])  

      const query = useStaticQuery(graphql`
      query {
          allExperience {
             edges{
                  node {
                      title
                      desc
                      date    
                      company
                  }      
              }       
          }    
      }
      `)

  return (
    <Layout>
      <MainContent>
        {query.allExperience.edges
          .map(({ node }) => (
            <div key={node.id}>
              <TitleSection>
                <MarkerHeader>{node.title}</MarkerHeader>
                <Link
                  to={node.title}
                  css={css`
                    color: inherit;
                  `}>
                </Link>
                <div>
                  <Company>{node.company}</Company><br></br>
                  <ArticleDate>{node.date}</ArticleDate>
                </div>
              </TitleSection>
              <Description>
                <div><p>{node.desc}</p></div>
              </Description>
            </div>
          ))}
      </MainContent>
    </Layout>
  )
}

export default IndexPage
