import PropTypes from "prop-types"
import React from "react"
import * as Styled from './styles.ts';

const Header = () => (
  <Styled.SiteHeader>
    <Styled.Content>
      <p>
        <Styled.HomeLink to="/">Home</Styled.HomeLink>
        <Styled.NavLink to="/resume">Resume</Styled.NavLink>
        <Styled.BitBucketLink target="_blank" href="https://bitbucket.org/Sarankhuu/gatsby-portfolio/src/master/">
          BitBucket
        </Styled.BitBucketLink>
      </p>
    </Styled.Content>
  </Styled.SiteHeader>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header;