import React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"

import Header from "../header/header"
import * as Styled from './styles.ts';
import "./layout.css"

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <Header siteTitle={data.site.siteMetadata.title} />
        <Styled.Content>
          <main>{children}</main>
          <Styled.Footer>
            <p>
            © {new Date().getFullYear()}, Built with
            {` `}
            </p>
            <Styled.GatsbyLink href="https://www.gatsbyjs.org">Gatsby</Styled.GatsbyLink>
          </Styled.Footer>
        </Styled.Content>
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
