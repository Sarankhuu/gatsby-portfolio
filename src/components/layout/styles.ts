import styled from "@emotion/styled"

export const Content = styled.div`
  margin: 0 auto;
  max-width: 860px;
  padding: 0 1.0875rem 1rem;
  padding-top: 0;
`;

export const GatsbyLink = styled.a`
  margin-left: 5px;
`;

export const Footer = styled.footer`
  display: flex;
  justify-content: center;
`;