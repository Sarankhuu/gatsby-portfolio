import React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"
import * as Styled from './styles.ts';

const LandingBio = () => (
  <StaticQuery
    query={graphql`
      query LandingSiteTitleQuery {
        site {
          siteMetadata {
            title
            subtitle
          }
        }
      }
    `}
    render={data => (
      <Styled.OuterContainer>
        <Styled.Container>
          <Styled.NameHeader>{data.site.siteMetadata.title}</Styled.NameHeader>
          <Styled.Description>{data.site.siteMetadata.subtitle}</Styled.Description>
        </Styled.Container>
      </Styled.OuterContainer>
    )}
  />
)

Styled.NameHeader.propTypes = {
  siteTitle: PropTypes.string,
  subtitle: PropTypes.string,
}

Styled.NameHeader.defaultProps = {
  siteTitle: ``,
  subtitle: ``,
}

export default LandingBio
