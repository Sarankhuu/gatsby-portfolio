module.exports = {
  siteMetadata: {
    title: `Sarankhuu B`,
    subtitle: `software developer`,
    description: `A minimal blog starter built with Gatsbyjs. The needed Gatsby files are included.`,
    author: `@saran`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-tailwindcss`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `markdown-pages`,
        path: `${__dirname}/src/content`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-emotion`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [`gatsby-remark-reading-time`, {
          resolve: `gatsby-remark-prismjs`,
          options: {
            aliases:{sh: "bash", js:"javascript"},
            showLineNumbers: true,
          }
        }],
      },
    },
    {
      resolve: `gatsby-plugin-netlify`,
      options: {
        headers: {
          "/*": [
            "Strict-Transport-Security: max-age=63072000"
          ]
        }, // option to add more headers. `Link` headers are transformed by the below criteria
        allPageHeaders: [], // option to add headers for all pages. `Link` headers are transformed by the below criteria
        mergeSecurityHeaders: true, // boolean to turn off the default security headers
        mergeLinkHeaders: true, // boolean to turn off the default gatsby js headers
        mergeCachingHeaders: true, // boolean to turn off the default caching headers
        transformHeaders: (headers, path) => headers, // optional transform for manipulating headers under each path (e.g.sorting), etc.
        generateMatchPathRewrites: true, // boolean to turn off automatic creation of redirect rules for client only paths
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`,
      },
    },
    {
      resolve: "gatsby-plugin-firebase",
      options: {
        credentials: {
          apiKey: "AIzaSyAeXri85JiWPlUvGu8iXile-HavTU0fcjA",
          authDomain: "fleet-impact-133206.firebaseapp.com",
          databaseURL: "https://fleet-impact-133206-default-rtdb.firebaseio.com",
          projectId: "fleet-impact-133206",
          storageBucket: "fleet-impact-133206.appspot.com",
          messagingSenderId: "59406323356",
          appId: "1:59406323356:web:cca5740d4375670d562759",
          measurementId: "G-Q4YR5ZZG39"
        },
        features: {
          firestore: true,
        }
      }
    },
    {
      resolve: "gatsby-source-firebase",
      options: {
        credential: require("./firebase_key.json"),
        databaseURL: "https://fleet-impact-133206-default-rtdb.firebaseio.com",
        types: [
          {
            type: "Experience",
            path: "experience",
          }
        ]
      }
    }
  ],
}
