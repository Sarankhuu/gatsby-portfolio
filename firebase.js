import firebase from 'firebase/app';
import 'firebase/auth'; // importing the auth module as an example

// Firebase web config
const config = {
	apiKey: "AIzaSyAeXri85JiWPlUvGu8iXile-HavTU0fcjA",
          authDomain: "fleet-impact-133206.firebaseapp.com",
          databaseURL: "https://fleet-impact-133206-default-rtdb.firebaseio.com",
          projectId: "fleet-impact-133206",
          storageBucket: "fleet-impact-133206.appspot.com",
          messagingSenderId: "59406323356",
          appId: "1:59406323356:web:cca5740d4375670d562759",
          measurementId: "G-Q4YR5ZZG39"
  // measurementId: '',
}

let instance = null;

export default function getFirebase() {
  if (typeof window !== 'undefined') {
    if (instance) return instance;
    instance = firebase.initializeApp(config);
    return instance;
  }

  return null;
}